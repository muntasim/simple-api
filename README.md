# README


* Ruby version
    * 2.6.3 

* Configuration
    * Rails 5.2.3
    * Sqlite3
    * Redis

* Database setup
    * rails db:create
    * rails db:migrate
    * rails db:seed

* How to run the test suite
    * rspec
    
# API Documentation

##  Reading


### Create a Reading [POST] [/api/v1/readings]

- Request (application/json)

    - Params  (household_token, reading{temperature, humidity, battery_charge})

- Response 201 (application/json)

    - Attributes (tracking_number:integer)

### Reading Resource [GET] [/api/v1/readings/{tracking_number}]
    
- Request (application/json)
- Params  (household_token)

- Response 200 (application/json)

    - Attributes (id, thermostat_id, tracking_number, temperature, humidity, battery_charge, created_at, updated_at)

### Reading Resource [GET] [/api/v1/readings/stats]
    
- Request (application/json)
- Params  (household_token)

- Response 200 (application/json)
    - stats
    - Attributes (average_temperature, minimum_humidity, maximum_battery_charge)
