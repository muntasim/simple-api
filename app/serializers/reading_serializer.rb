class ReadingSerializer < ActiveModel::Serializer
  type :reading
  attributes :id, :thermostat_id, :tracking_number, :temperature, :humidity, :battery_charge
end
