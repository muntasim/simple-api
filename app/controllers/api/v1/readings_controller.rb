class Api::V1::ReadingsController < ApplicationController
  include AuthenticateHousehold

  def create
    if valid_reading_params?
      ReadingWorker.enqueue(reading_params.to_h)
      json_response({ tracking_number: @next_tracking_number }, :created)
    else
      json_response({ message: "Invalid parameter" }, 422)
    end
  end

  def show
    reading = Reading.fetch_by_tracking_number(@thermostat.id, params[:tracking_number])
    if reading
      reading = Reading.new(reading) unless reading.is_a?(Reading)
      json_response(reading)
    else
      json_response({ message: "No data available" }, 404)
    end
  end

  def stats
    data = ReadingStats.generate(@thermostat.id)
    if data
      json_response({stats: data})
    else
      json_response({ message: "No data available" }, 404)
    end
  end

  private

  def valid_reading_params?
    reading_params = params[:reading] || {}
    [:temperature, :humidity, :battery_charge].all? { |a| reading_params[a].present? }
  end

  def reading_params
    _params = params.require(:reading).permit(:temperature, :humidity, :battery_charge)
    @next_tracking_number = @thermostat.next_tracking_number
    _params.merge!(tracking_number: @next_tracking_number, thermostat_id: @thermostat.id)
  end
end
