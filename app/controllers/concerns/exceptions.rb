module Exceptions
  extend ActiveSupport::Concern

  class AuthenticationError < StandardError
    def message
      "Authentication failed!"
    end
  end

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      json_response({ message: e.message }, :unprocessable_entity)
    end

    rescue_from AuthenticationError do |e|
      json_response({ message: e.message }, 401)
    end
  end
end
