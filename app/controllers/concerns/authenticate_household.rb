module AuthenticateHousehold
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_thermostat

    private

    def authenticate_thermostat
      @thermostat = Thermostat.find_by(household_token: params[:household_token])
      raise Exceptions::AuthenticationError unless @thermostat
    end
  end
end
