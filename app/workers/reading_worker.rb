class ReadingWorker
  include Sidekiq::Worker

  def perform(*attributes)
    Reading.create(*attributes)
    ReadingCache.new(*attributes).delete!
  end

  def self.enqueue(reading_params)
    ReadingWorker.perform_async(reading_params)
    ReadingCache.new(reading_params).add!
  end
end
