class Thermostat < ApplicationRecord
  has_many :readings
  validates :household_token, presence: true, uniqueness: true
  validates :location, presence: true

  before_validation :generate_token, on: :create

  def next_tracking_number
    (readings.last&.tracking_number || 0) + 1
  end

  protected

  def generate_token
    return true if household_token.present?
    self.household_token = loop do
      random_token = SecureRandom.urlsafe_base64(99, false)
      break random_token unless Thermostat.exists?(household_token: random_token)
    end
  end
end
