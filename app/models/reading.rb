class Reading < ApplicationRecord
  belongs_to :thermostat
  validates :tracking_number, presence: true, uniqueness: true, numericality: { only_integer: true }
  validates :temperature, presence: true, numericality: true
  validates :humidity, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }
  validates :battery_charge, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  before_validation :set_tracking_number, on: :create

  def self.fetch_by_tracking_number(thermostat_id, tracking_number)
    find_by(thermostat_id: thermostat_id, tracking_number: tracking_number) ||
        ReadingCache.fetch(thermostat_id, tracking_number)
  end

  protected

  def set_tracking_number
    return true unless thermostat # lets the validation take care of that
    return true if tracking_number && thermostat.readings.where(tracking_number: tracking_number).none?
    self.tracking_number = (thermostat.readings.last&.tracking_number || 0) + 1
  end
end
