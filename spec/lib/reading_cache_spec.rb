require 'rails_helper'

RSpec.describe ReadingCache do
  let(:reading) { { thermostat_id: 1, tracking_number: 2, humidity: 1.3, temperature: 3.5, battery_charge: 20.0 } }
  let(:redis_cache) { ReadingCache.new(reading) }
  before do
    $redis.flushall
  end

  describe "#add!" do
    it 'should add to cache' do
      redis_cache.add!
      expect($redis.hgetall(redis_cache.send(:reading_key))["thermostat_id"]).to eq "1"
      expect($redis.hgetall(redis_cache.send(:reading_key))["tracking_number"]).to eq "2"
    end
  end

  describe "#delete!" do
    it 'should remove' do
      redis_cache.add!
      expect($redis.hgetall(redis_cache.send(:reading_key))["thermostat_id"]).to eq "1"
      redis_cache.delete!
      expect($redis.hgetall(redis_cache.send(:reading_key))).to eq({})
    end
  end

  describe ".summary" do
    it 'should return all thermostat data' do
      reading1 = { thermostat_id: 1, tracking_number: 2, humidity: 1.3, temperature: 3.5, battery_charge: 50.0 }
      reading2 = { thermostat_id: 1, tracking_number: 3, humidity: 5.3, temperature: 8.5, battery_charge: 20.0 }
      reading3 = { thermostat_id: 3, tracking_number: 1, humidity: 1.3, temperature: 3.5, battery_charge: 20.0 }

      ReadingCache.new(reading1).add!
      ReadingCache.new(reading2).add!
      ReadingCache.new(reading3).add!
      expected_summary = { count: 2, average_temperature: 6.0, minimum_humidity: 1.3, maximum_battery_charge: 50.0 }
      expect(ReadingCache.summary(1)).to eq(expected_summary)
    end

    it 'should return 0 with no data' do
      expected_summary = { count: 0 }
      expect(ReadingCache.summary(1)).to eq(expected_summary)
    end
  end

end
