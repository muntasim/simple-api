require 'rails_helper'

RSpec.describe ReadingStats do
  let(:thermostat) { create(:thermostat)}

  before do
    $redis.flushall
  end

  describe ".generate" do
    it 'should call stats' do
      expect(ReadingStats).to receive_message_chain(:new, :stats)

      ReadingStats.generate(thermostat.id)
    end
  end

  describe ".stats" do
    context "with data" do
      before do
        create :reading, thermostat: thermostat, temperature: 10, humidity: 40.5, battery_charge: 20.5
        create :reading, thermostat: thermostat, temperature: 30, humidity: 30.5, battery_charge: 40.5
        create :reading, thermostat: thermostat, temperature: 20, humidity: 60.5, battery_charge: 10.5
      end

      it 'should return calculated result' do
        stats = ReadingStats.generate(thermostat.id)
        expect(stats[:average_temperature]).to eq 20.0
        expect(stats[:minimum_humidity]).to eq 30.5
        expect(stats[:maximum_battery_charge]).to eq 40.5
      end

      it 'should consider cache data' do
        reading4 = { thermostat_id: thermostat.id, tracking_number: 4, humidity: 10.3, temperature: 25, battery_charge: 50.0 }
        reading5 = { thermostat_id: thermostat.id, tracking_number: 5, humidity: 11.3, temperature: 20, battery_charge: 20.0 }
        ReadingCache.new(reading4).add!
        ReadingCache.new(reading5).add!

        stats = ReadingStats.generate(thermostat.id)

        expect(stats[:average_temperature]).to eq 21.0
        expect(stats[:minimum_humidity]).to eq 10.3
        expect(stats[:maximum_battery_charge]).to eq 50.0
      end
    end

    context "without data" do
      it 'should return nil/zero ' do
        stats = ReadingStats.generate(thermostat.id)
        expect(stats).to eq nil
      end
    end
  end

end
