require 'rails_helper'

RSpec.describe ReadingWorker do
  describe "#perform" do
    it 'creates Reading' do
      attrs = { thermostat_id: 1, tracking_number: 2, humidity: 1.3, temperature: 3.5, battery_charge: 20.0 }
      expect(Reading).to receive(:create).with(attrs)
      expect_any_instance_of(ReadingCache).to receive(:delete!)
      ReadingWorker.new.perform(attrs)
    end
  end

  describe ".enqueue" do
    it 'perform async' do
      attrs = { thermostat_id: 1, tracking_number: 2, humidity: 1.3, temperature: 3.5, battery_charge: 20.0 }
      expect(ReadingWorker).to receive(:perform_async).with(attrs)
      expect_any_instance_of(ReadingCache).to receive(:add!)

      ReadingWorker.enqueue(attrs)
    end
  end

end
