require 'rails_helper'

RSpec.describe Reading, type: :model do
  describe "associations" do
    it { should belong_to(:thermostat) }
  end

  describe "validations" do
    it { should validate_uniqueness_of(:tracking_number) }
    it { should validate_presence_of(:tracking_number) }
    it { should validate_presence_of(:temperature) }
    it { should validate_presence_of(:humidity) }
    it { should validate_presence_of(:battery_charge) }
    it { should validate_numericality_of(:tracking_number).only_integer }
    it { should validate_numericality_of(:temperature) }
    it { should validate_numericality_of(:humidity).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(100) }
    it { should validate_numericality_of(:battery_charge).is_greater_than_or_equal_to(0).is_less_than_or_equal_to(100) }
  end

  describe ".create" do
    context "no tracking_number given" do
      it 'should set tracking_number' do
        reading = FactoryBot.create(:reading, tracking_number: nil)
        expect(reading.tracking_number.present?).to be_truthy
        expect(reading.tracking_number).to eq 1
      end
    end

    context "with tracking_number given" do
      it 'should take tracking_number' do
        reading = FactoryBot.create(:reading, tracking_number: 1001)
        expect(reading.tracking_number).to eq 1001
      end
    end
  end

  before do
    $redis.flushall
  end

  describe ".fetch_by_tracking_number" do
    let(:thermostat) { FactoryBot.create(:thermostat) }

    context "from database" do
      it 'should return reading' do
        reading = create :reading, thermostat: thermostat, temperature: 10, humidity: 40.5, battery_charge: 20.5
        data = Reading.fetch_by_tracking_number(thermostat.id, reading.tracking_number)
        expect(data.present?).to be_truthy
        expect(data.is_a?(Reading)).to be_truthy
      end
    end

    context "from cache" do
      it 'should return reading hash' do
        reading = { thermostat_id: thermostat.id, tracking_number: 4, humidity: 10.3, temperature: 25, battery_charge: 50.0 }
        ReadingCache.new(reading).add!
        data = Reading.fetch_by_tracking_number(thermostat.id, 4)
        expect(data.present?).to be_truthy
        expect(data.is_a?(Hash)).to be_truthy
      end
    end
    context "no data" do
      it 'should return nil' do
        data = Reading.fetch_by_tracking_number(1111, 1212)
        expect(data.present?).to be_falsey
      end
    end
  end
end
