require "rails_helper"

RSpec.describe Thermostat, type: :model do
  describe "association" do
    it { should have_many(:readings) }
  end

  describe "validations" do
    it { should validate_uniqueness_of(:household_token) }
    it { should validate_presence_of(:location) }
  end

  describe ".create" do
    context "no household_token given" do
      it 'should set household_token' do
        thermostat = FactoryBot.create(:thermostat, household_token: nil)
        expect(thermostat.household_token.present?).to be_truthy
        expect(thermostat.household_token.length).to eq 132
      end
    end

    context "with household_token given" do
      it 'should take household_token' do
        random_token = SecureRandom.urlsafe_base64(99, false)
        thermostat = FactoryBot.create(:thermostat, household_token: random_token)
        expect(thermostat.household_token).to eq(random_token)
      end
    end
  end

  describe "#next_tracking_number" do
    it 'should return 1 for the first' do
      expect(Thermostat.new.next_tracking_number).to eq 1
    end

    it 'should return 4' do
      thermostat = FactoryBot.create(:thermostat)
      create_list :reading, 3, thermostat: thermostat
      expect(thermostat.next_tracking_number).to eq 4
    end
  end
end
