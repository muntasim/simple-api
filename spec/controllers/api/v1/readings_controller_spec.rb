require 'rails_helper'

RSpec.describe Api::V1::ReadingsController, type: :controller do
  let(:thermostat) { create(:thermostat) }

  describe "POST #create" do
    context "with valid attributes" do
      let(:valid_attributes) { attributes_for(:reading) }
      it "create a new reading" do
        tracking_number = thermostat.next_tracking_number
        post :create, params: { household_token: thermostat.household_token, reading: valid_attributes }
        response_body = JSON.parse(response.body)
        expect(response_body["tracking_number"]).to eq tracking_number
        expect(response.status).to eq(201)
      end
    end
  end

  before do
    $redis.flushall
  end

  describe "GET #stats" do
    it "returns stats" do
      create_list(:reading, 10, thermostat: thermostat)
      get :stats, params: { household_token: thermostat.household_token }

      response_body = JSON.parse(response.body)["stats"]
      expect(response_body["average_temperature"]).to eq 1.5
      expect(response_body["minimum_humidity"]).to eq 41.5
      expect(response_body["maximum_battery_charge"]).to eq 34.5
      expect(response.status).to eq(200)
    end

    context "No data" do
      it "returns 401" do
        get :stats, params: { household_token: thermostat.household_token }
        expect(response.status).to eq(404)
      end
    end
  end

  describe "GET #show" do
    it "returns reading" do
      reading = create(:reading, thermostat: thermostat)
      get :show, params: { id: reading.tracking_number, household_token: thermostat.household_token, tracking_number: reading.tracking_number }
      reading_response = JSON.parse(response.body)["reading"]
      match_reading(reading_response, reading)
      expect(response.status).to eq(200)
    end

    it "returns reading if not created yet" do
      tracking_number = thermostat.next_tracking_number
      post :create, params: { household_token: thermostat.household_token, reading: attributes_for(:reading) }
      get :show, params: { id: tracking_number, household_token: thermostat.household_token, tracking_number: tracking_number }
      reading_response = JSON.parse(response.body)["reading"]
      match_reading(reading_response, Reading.new(attributes_for(:reading, tracking_number: tracking_number)))
      expect(response.status).to eq(200)
    end
  end

  def match_reading(reading_response, reading)
    expect(reading_response["thermostat_id"]).to eq thermostat.id
    expect(reading_response["tracking_number"]).to eq reading.tracking_number
    expect(reading_response["temperature"]).to eq reading.temperature
    expect(reading_response["humidity"]).to eq reading.humidity
  end
end
