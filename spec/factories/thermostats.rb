FactoryBot.define do
  factory :thermostat do
    sequence(:household_token, 101) { |n| "Token#{n}" }
    location { Faker::Address.full_address }
  end
end
