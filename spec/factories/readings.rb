FactoryBot.define do
  factory :reading do
    thermostat { build(:thermostat) }
    temperature { 1.5 }
    humidity { 41.5 }
    battery_charge { 34.5 }
  end
end
