# Create thermostats
1.upto(10) do |i|
  Thermostat.create(location: Faker::Address.full_address)
end
puts "Thermostats created"


# Create readings
thermostat_ids = Thermostat.pluck(:id)
1.upto(100) do |i|
  Reading.create(
      thermostat_id: thermostat_ids.shuffle.first,
      temperature: rand(11.2...76.9),
      humidity: rand(20.2...86.9),
      battery_charge: rand(0.2...100.0),
  )
end

puts "Readings created"
puts "Seeding completed!"



