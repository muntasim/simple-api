Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    api_version(module: 'v1', path: { value: 'v1' }, default: true) do
      resources :readings, only: [:show, :create] do
        collection do
          get :stats
        end
      end
    end
  end
end
