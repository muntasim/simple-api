class ReadingStats

  attr_accessor :thermostat_id, :db_stats, :cached_stats

  def initialize(thermostat_id)
    @thermostat_id = thermostat_id
    @db_stats = fetch_data
    @cached_stats = ReadingCache.summary(thermostat_id)
  end

  def stats
    return nil unless cache_data_exist? || db_stats
    {
        average_temperature: average_temperature,
        minimum_humidity: minimum_humidity,
        maximum_battery_charge: maximum_battery_charge
    }
  end

  private

  def cache_data_exist?
    cached_stats[:count] > 0
  end

  def average_temperature
    if cache_data_exist?
      (db_stats[1].to_f * db_stats[0].to_i + cached_stats[:average_temperature] * cached_stats[:count]) / (db_stats[0].to_i + cached_stats[:count])
    else
      db_stats[1]
    end
  end

  def minimum_humidity
    if cache_data_exist?
      [db_stats[2].to_f, cached_stats[:minimum_humidity]].min
    else
      db_stats[2]
    end
  end

  def maximum_battery_charge
    if cache_data_exist?
      [db_stats[3].to_f, cached_stats[:maximum_battery_charge]].max
    else
      db_stats[3]
    end
  end

  def fetch_data
    query = <<-SQL
      SELECT count(id) count, avg(temperature) avg_temperature, min(humidity) min_humidity, max(battery_charge) max_battery_charge FROM readings 
      WHERE readings.thermostat_id = ? GROUP BY thermostat_id
    SQL

    connection = ActiveRecord::Base.connection.raw_connection.prepare(query)
    db_stats = connection.execute(thermostat_id).first
    connection.close
    db_stats
  end


  def self.generate(thermostat_id)
    new(thermostat_id).stats
  end
end
