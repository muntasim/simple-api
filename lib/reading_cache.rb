class ReadingCache

  attr_accessor :reading_attributes

  def initialize(reading_attributes)
    @reading_attributes = reading_attributes
  end

  def add!
    $redis.mapped_hmset(reading_key, reading_attributes)
  end

  def delete!
    $redis.del(reading_key)
  end

  def self.fetch(thermostat_id, tracking_number)
    $redis.hgetall("thermostat-#{thermostat_id}:#{tracking_number}")
  end

  def self.summary(thermostat_id)
    values = $redis.keys("thermostat-#{thermostat_id}*").map { |key| $redis.hgetall(key) }
    count = values.size
    if count > 0
      { count: count,
        average_temperature: values.map { |c| c["temperature"].to_f }.sum.fdiv(count),
        minimum_humidity: values.map { |c| c["humidity"].to_f }.min,
        maximum_battery_charge: values.map { |c| c["battery_charge"].to_f }.max }
    else
      { count: 0 }
    end

  end

  private

  def thermostat_id
    reading_attributes[:thermostat_id]
  end

  def tracking_number
    reading_attributes[:tracking_number]
  end

  def reading_key
    "thermostat-#{thermostat_id}:#{tracking_number}"
  end
end
